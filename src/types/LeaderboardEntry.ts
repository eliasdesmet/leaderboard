export interface LeaderboardEntry {
    id?: string;
    leaderboardId?: string;
    winner?: string;
    loser?: string;
    datePlayed?: Date;
};