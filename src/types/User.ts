import { Timestamp } from "firebase/firestore";

export interface User {
    id?: string;
    username?: string;
    approved?: boolean;
    role?: string;
    dateJoined?: Timestamp;
    dateLastLogin?: Timestamp;
};