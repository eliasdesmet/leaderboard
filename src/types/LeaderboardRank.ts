export interface LeaderboardRank {
    id: string;
    username: string;
    wins: number;
    losses: number;
    lastWinDate: Date;
    gamesPlayed?: number;
    winPercentage?: number;
};