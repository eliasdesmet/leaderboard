import { Timestamp } from "firebase/firestore";

export interface Leaderboard {
    id?: string;
    name?: string;
    dateCreated?: Timestamp;
    dateLastUpdated?: Timestamp;
};