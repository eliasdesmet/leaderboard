export const PageTitle = (prefix: string | null) => {
    const delimiter = '-'
    const defaultTitle = document.title.split(delimiter)[document.title.split(delimiter).length - 1];

    if (prefix) {
        document.title = `${prefix} ${delimiter} ${defaultTitle}`;
    } else {
        document.title = defaultTitle;
    }
}